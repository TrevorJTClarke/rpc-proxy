require('dotenv').config()
import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import bodyParser from 'body-parser'
import { createServer } from 'http'
import rpcHandler from './rpcProxy'

const port = process.env.PORT || 4242
const app = express()

app.use(cors())
app.use(helmet())
app.use(bodyParser.json())

app.post('/', rpcHandler)

const server = createServer(app)
server.listen({ port }, async () => {
  console.log(`🤖 Server ready at http://localhost:${port}`)
})