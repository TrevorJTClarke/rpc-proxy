import { readFileSync, writeFileSync } from 'fs'
import path from 'path'
import { JSONRPCServer } from 'json-rpc-2.0'
import config, { RPC_METHODS } from './config'
import axios from 'axios'

const rpc = new JSONRPCServer()
const env = process.env.NODE_ENV
const proxyRPC = config[env].RPC_URL
const basePath = path.join(__dirname, '../output')

const getFilePath = (file, id) => (path.join(basePath, `${file}_${id}.json`))

const getFile = async (file, id) => {
    try {
        const res = await readFileSync(getFilePath(file, id), 'utf-8')
        return JSON.parse(res)
    } catch (e) {
        return
    }
}

const setFile = async (file, data) => {
    try {
        return writeFileSync(getFilePath(file, data.id), JSON.stringify(data), 'utf-8')
    } catch (e) {
        return
    }
}

const proxyRequest = async args => {
    try {
        const res = await axios.post(proxyRPC, args)
        if (res.data && res.data.result) return res.data
    } catch (e) {
        return
    }
}

const methodHandler = async args => {
    // first try local cache, if any return!
    const file = await getFile(args.method, args.id)
    if (file) return file

    // no cache, proxy call
    const proxyData = await proxyRequest(args)

    // cache any response
    if (proxyData) await setFile(args.method, proxyData)

    // return!
    return proxyData
}

// load all configured methods
RPC_METHODS.forEach(method => {
    rpc.addMethodAdvanced(method, methodHandler)
})

export default async function rpcHandler(req, res) {
    const payload = await rpc.receive(req.body)

    if (payload) res.json(payload)
    else res.sendStatus(204)
}