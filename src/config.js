require('dotenv').config()

export const RPC_METHODS = [
  'block',
  'chunk',
  'gas_price',
  'network_info',
  'query',
  'status',
  'tx',
  'EXPERIMENTAL_tx_status',
  'validators',
]

export default {
  development: {
    RPC_URL: process.env.DEV_RPC_URL || '',
  },
  production: {
    RPC_URL: process.env.PROD_RPC_URL || '',
  },
}