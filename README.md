# RPC Proxy

Configurable JSON RPC 2.0 Proxy with support for replay & recording -- makes E2E testing easy!

## Install
```
npm i
```

## USE

#### Configure:
```
cp .env.example .env
// Change RPC details to what you need
```

#### Run:
```
npm start
```

#### Clear Cache:
```
npm run clear
```